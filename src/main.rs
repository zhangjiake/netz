use std::{
    fs,
    io::{prelude::*, BufReader},
    net::{TcpListener, TcpStream},
    path::Path,
};
use zhangjiakenetz::ThreadPool;
use regex::Regex;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:7878").expect("main TcpListener::bind failed");
    let pool = ThreadPool::new(4);
    for stream in listener.incoming() {
        let stream = stream.expect("listener incoming should not have error");
        pool.execute(|| {
            handle_connection(stream);
        });
    }
    println!("shutting down main");
}

fn handle_connection(mut stream: TcpStream) {
    let buf_reader = BufReader::new(&mut stream);
    let request_line = buf_reader.lines().next().expect("[handle_connection] read next line failed");
    if request_line.is_err() {
        return println!("idk why but there might be an issue in the request line. discarding this request.");
    }
    let request_line = request_line.unwrap();
    let request_path_re = Regex::new(r"^GET /(.*) HTTP/1.1$").unwrap();
    let request_cap = request_path_re.captures(&request_line);
    let request_target = match request_cap {
        None => {
            return respond_404_not_found(&mut stream);
        },
        Some(c) => c.get(1).unwrap().as_str(),
    };
    // dot dot slash attack
    if request_target.contains("..") {
        return respond_404_not_found(&mut stream);
    }
    if request_target == "" {
        return respond_with_file(&mut stream, Path::new("oeffentlich/seiten/index.html"));
    }
    let request_path = Path::new(request_target);
    if !request_path.starts_with("oeffentlich") || !request_path.exists() {
        return respond_404_not_found(&mut stream);
    }
    if request_path.is_file() {
        return respond_with_file(&mut stream, &request_path);
    }
    respond_404_not_found(&mut stream)
/*
    if request_path == "/schriften" {
        let contents = get_list_schriften_content();
        respond_200_ok(&mut stream, &contents);
        return;
    }
    let md_re = Regex::new(r"^/schriften/(\d{8})$").unwrap();
    let contents = match md_re.captures(&request_path) {
        Some(caps) => try_get_md_content(&caps[1]),
        None => get_static_content("index"),
        // "GET / HTTP/1.1" => ("HTTP/1.1 200 OK", "index.html"),
        // _ => ("HTTP/1.1 404 NOT FOUND", "404.html"),
    };
*/
}

fn respond_200_ok(mut stream: &TcpStream, content: Vec<u8>, content_type: &str) {
    let status_line = "HTTP/1.1 200 OK";
    //let length = content.len();
    let mut response = format!(
        //"{status_line}\r\nContent-Length: {length}\r\n\r\n{content}"
        "{status_line}\r\nContent-Type: {content_type}\r\n\r\n"
    ).into_bytes();
    response.extend(content);
    stream.write_all(&response).expect("[respond_200_ok] stream write_all failed")
}

fn respond_404_not_found(mut stream: &TcpStream) {
    let status_line = "HTTP/1.1 404 NOT FOUND";
    let content = "<h1>404 NOT FOUND</h1>";
    let length = content.len();
    let response = format!(
        "{status_line}\r\nContent-Length: {length}\r\n\r\n{content}"
    );
    stream.write_all(response.as_bytes()).expect("[respond_404_not_found] stream write_all failed")
}

fn respond_with_file(mut stream: &TcpStream, path: &Path) {
    let path_str = path.to_str().unwrap();
    let content = fs::read(path_str).expect("respond_with_file read failed");
	let content_type = match path.extension() {
		None => "",
		Some(os_str) => match os_str.to_str().unwrap() {
			"html" => "text/html",
			"js" => "text/javascript",
			_ => ""
		}
	};
    respond_200_ok(&mut stream, content, content_type)
}

/*
fn get_list_schriften_content() -> String {
    let list_items: String = Path::new("schriften")
        .read_dir()
        .unwrap()
        .map(|dir_entry_result| {
            let name = dir_entry_result.unwrap().file_name().into_string().unwrap();
            format!("<li>{name}</li>")
        })
        .collect();
    let body_content = format!("<ul>{list_items}</ul>");
    render_body_content_as_page("Schriften", &body_content)
}
fn try_get_md_content(filedate: &str) -> String {
    let schriften_path = Path::new("schriften");
    for entry in schriften_path.read_dir().unwrap() {
        let entry = entry.unwrap();
        let entry_name = entry.file_name().into_string().unwrap();
        if entry_name.starts_with(filedate) {
            let this_file_path = entry.path();
            let md_to_html = markdown::file_to_html(&this_file_path).unwrap();
            return render_body_content_as_page(&entry_name, &md_to_html);
        }
    }
    get_static_content("index")
}

fn render_body_content_as_page(title: &str, body_content: &str) -> String {
    format!(r"<!DOCTYPE html>
<html>
    <head>
        <title>{title}</title>
        <meta charset='utf-8' />
    </head>
    <body>
        {body_content}
    </body>
</html>")
}
*/
