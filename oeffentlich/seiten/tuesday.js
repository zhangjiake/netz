import { computed, ref } from 'vue'
export default {
	setup() {
		const day = ref("tuesday")
		const message = computed(() => `it's ${day.value} my dudes`)
		function toggle() {
			day.value = day.value === "tuesday" ? "wednesday" : "tuesday"
		}
		return {
			message,
			toggle
		}
	}
}
